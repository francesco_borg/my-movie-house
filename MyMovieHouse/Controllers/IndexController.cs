﻿using System.IO;
using System.Collections.Generic;
using System.Web.Mvc;
using MyMovieHouse.Models;
using MyMovieHouse.Logic;
namespace MyMovieHouse.Controllers
{
    public class IndexController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(string moviePath)
        {
            List<Movie> movieList = new List<Movie>();
            Movie_Logic logic = new Movie_Logic();

            string[] movieNames = Directory.GetDirectories(moviePath);
            for (int i = 0; i < movieNames.Length; i++)
            {
                Movie movie = new Movie();
                movie = logic.getMovieFromPath(Path.GetFileName(movieNames[i]),movie);
                movieList.Add(movie);
            }
            return View(movieList);
        }
    }
}