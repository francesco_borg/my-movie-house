﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyMovieHouse.Models
{
    public class Movie
    {
        public string Title { get; set; }
        public string Released { get; set; }
        public string Genre { get; set; }
        public string Poster { get; set; }
    }
}