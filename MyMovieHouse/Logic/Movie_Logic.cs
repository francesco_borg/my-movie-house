﻿using System.Net;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using MyMovieHouse.Logic;
using MyMovieHouse.Models;
using System.Text.RegularExpressions;
using System;

namespace MyMovieHouse.Logic
{
    public class Movie_Logic
    {
        public Movie getMovieFromPath(string movieName, Movie movie)
        {

            movie = getMovieInfoFromJson(getMovieData(movieName),movie);
            return movie;
        }

        private Movie getMovieInfoFromJson(object jsonData, Movie m)
        {
            JObject movieJson = JObject.Parse(jsonData.ToString());
            m.Title = movieJson["Title"].ToString();
            m.Released = movieJson["Released"].ToString();
            m.Genre = movieJson["Genre"].ToString();
            m.Poster = movieJson["Poster"].ToString();

            return m;
        }

        private string getMovieData(string movieName)
        {
            const string apiKey = "2d4146c5";
            string movieWebsite = "https://www.omdbapi.com/?t=" + movieName + "&apikey=" + apiKey;
            var json = string.Empty;

            using (var data = new WebClient())
            {
                json = data.DownloadString(movieWebsite);
            }

            return json;
        }


        

    }
}